var zipFolder = require('zip-folder');
var path = require('path');
var fs = require('fs');
var request = require('request');

var rootFolder = path.resolve('.');
var zipPath = path.resolve(rootFolder, '../dev01-vkbbot01.zip');
var kuduApi = 'https://dev01-vkbbot01.scm.azurewebsites.net/api/zip/site/wwwroot';
var userName = '$dev01-vkbbot01';
var password = 'KH3j0W4R5RY0Dv924ZmTK0dWDolldtBimRCtlvWPtQY4daybbkFCHJQDEpnE';

function uploadZip(callback) {
  fs.createReadStream(zipPath).pipe(request.put(kuduApi, {
    auth: {
      username: userName,
      password: password,
      sendImmediately: true
    },
    headers: {
      "Content-Type": "applicaton/zip"
    }
  }))
  .on('response', function(resp){
    if (resp.statusCode >= 200 && resp.statusCode < 300) {
      fs.unlink(zipPath);
      callback(null);
    } else if (resp.statusCode >= 400) {
      callback(resp);
    }
  })
  .on('error', function(err) {
    callback(err)
  });
}

function publish(callback) {
  zipFolder(rootFolder, zipPath, function(err) {
    if (!err) {
      uploadZip(callback);
    } else {
      callback(err);
    }
  })
}

publish(function(err) {
  if (!err) {
    console.log('dev01-vkbbot01 publish');
  } else {
    console.error('failed to publish dev01-vkbbot01', err);
  }
});